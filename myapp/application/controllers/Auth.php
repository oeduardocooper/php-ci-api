<?php
   
   require APPPATH . '/libraries/REST_Controller.php';
   use Restserver\Libraries\REST_Controller;
     
class Auth extends REST_Controller {

    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * gera o token
     *
     * @return Response
    */
	public function index_post() {

        $this->load->model('UserModel');

        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if (!$username && !$email) {
            $data = [
                "status" => false,
                "message" => 'Username or e-mail is mandatory',
            ];
            $this->response($data, REST_Controller::HTTP_OK);
        }

        if (!$password) {
            $data = [
                "status" => false,
                "message" => 'Password is mandatory',
            ];
            $this->response($data, REST_Controller::HTTP_OK);
        }
        
        $user = $this->UserModel->getUser($username, $email);

        if(empty($user)) {
            $data = [
                "status" => false,
                "message" => 'User not found',
            ];
            $this->response($data, REST_Controller::HTTP_OK);            
        }

        
        $this->load->library('encryption');
        $pwd = $this->encryption->decrypt($user[0]->password);
        if($password !== $pwd) {
            $data = [
                "status" => false,
                "message" => 'Incorrect password',
            ];
            $this->response($data, REST_Controller::HTTP_OK);               
        }

        $this->load->helper('jwt');
        $jwt = new JWT();

        $payload = array(
            'id' => $user[0]->id,
            'name' => $user[0]->name,
            'username' => $user[0]->username,
            'email' => $user[0]->email,
        );
     
        $token = $jwt->encode($payload, JWT_KEY);

        $data = [
            'status' => true,
            'token' => $token,
            'message' => 'Token obtained successfully',
        ];

        $this->response($data, REST_Controller::HTTP_OK);
	}
    	
}