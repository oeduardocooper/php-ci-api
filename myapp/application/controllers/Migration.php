<?php
   
   require APPPATH . '/libraries/REST_Controller.php';
   use Restserver\Libraries\REST_Controller;
     
class Migration extends REST_Controller {
    
	  /**
     * construtor
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * cria as tabelas
     *
     * @return Response
    */
	public function index_get()
	{
        $this->load->library('encryption');

        try {
            $sql = 'drop table if exists user;';
            $this->db->query($sql);
    
            $sql = '
                create table user(
                    id int not null auto_increment,
                    name varchar(255) not null,
                    username varchar(255) not null,
                    password varchar(255) not null,
                    email varchar(255) not null,
                    primary key ( id )
                );
            ';
            $this->db->query($sql);

            $pwd = $this->encryption->encrypt('123123');
            $sql = "insert into user(name, username, password, email) values ('Eduardo Cooper', 'educooper', '{$pwd}', 'oeduardocooper@gmail.com');";            
            $this->db->query($sql);

            $data = [
                "status" => true,
                "message" => "migração realizada com sucesso",
            ];
         
            $this->response($data, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $data = [
                "status" => false,
                "message" => "ocorreu um erro ao executar a migração",
                "error" => $e->getMessage()
            ];
            $this->response($data, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }


	}

    	
}