# CI API

Api em PHP com framework CodeIgniter e Docker.

## Passo a passo

para executar a api

```bash
make up
```

para encerrar a api

```bash
make stop
```

ou
```bash
make kill
```

## consumindo web

```bash
http://localhost:8000/
```

## adminer MariaDB
###### server: mariadb
###### port: 3306
###### database: apidb
###### user: admin
###### password: nimda

```bash
http://localhost:8080
```

## executando o migration (criando o DB)

```
GET http://localhost:8000/migration
```